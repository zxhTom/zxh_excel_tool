简介：	
	Javaweb开发中数据的导入导出很常见，每次我们都需要写很多代码，所以我就在想能不能写一些通用的方法，之前已经在网上
	整理过一些通用的方法，最近在网上看到一位牛人封装的更加简介，自己拿过来整理了一下。

基本环境：
	该项目在jdk1.7上搭配编写
	
所需jar:
	log4j-1.2.17.jar 日志jar包
	servlet-api.jar  正常在web项目中都不需要引入该jar包
	poi-3.9.jar	             操作表格jar包
	pi-ooxml-3.12.jar XSSFWorkbook 处理2007版本及以上的excel
	
	commons-beanutils-1.8.3.jar 主要处理bean属性操作
	
结构：
	tom.zhangxinhua.base   :主要放置Excel导出导入的基本配置类
	tom.zhangxinhua.expose :将封装好的方法暴露给使用者使用